# Indico Widget Plugin

Base code from the WPExplorer.com tutorial: 
- [How To Create A Widget Plugin For WordPress](http://www.wpexplorer.com/create-widget-plugin-wordpress/)
- [GitHub repo](https://github.com/wpexplorer/my-widget-plugin/blob/master/my-widget-plugin.php)

Autoupdate system based on 
- [https://github.com/YahnisElsts/plugin-update-checker](https://github.com/YahnisElsts/plugin-update-checker)

- How to release an update:
    - Update Version header in main file indico-widget-plugin.php
    - Update the 'stable-release' branch in gitlab.
    